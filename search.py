from settings import Settings
import requests
import rts_podcast

headers = {
        'Authorization' : f'Bearer {rts_podcast.get_token()}',
        'Accept' : 'application/json'
    }

parameters = {
    'bu' : 'rts',
    'q' : 'cqfd'
}

results = requests.get(url=f'{rts_podcast.BASE_URL}/radioshows/search', headers=headers, params=parameters)

print(results.json()['searchResultListShow'])