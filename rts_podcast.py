from settings import Settings
import requests
import base64
import podgen
from podgen import Podcast, Media

BU = 'rts'
BASE_URL = 'https://api.srgssr.ch/audiometadata/v2'


class Show():
    def __init__(self, title: str, description: str, url: str, ) -> None:
        self.title = title
        self.description = description
        self.url = url


class Episode():
    def __init__(self, show: Show, medias: list) -> None:
        self.show = show
        self.medias = medias


def get_token():
    # TODO caching
    headers = {
        'Authorization': base64.b64encode(f'{Settings.KEY}:{Settings.SECRET}'.encode('ascii')),
        'Cache-Control': 'no-cache',
        'Content-Length': '0',
        'Postman-Token': '24264e32-2de0-f1e3-f3f8-eab014bb6d76'
    }

    res = requests.post(
        'https://api.srgssr.ch/oauth/v1/accesstoken?grant_type=client_credentials', headers=headers)

    return res.json()['access_token']


def get_first_media_id(show_id: str) -> str:
    headers = {
        'Authorization': f'Bearer {get_token()}',
        'Accept': 'application/json'
    }

    parameters = {
        'bu': BU
    }

    results = requests.get(
        url=f'{BASE_URL}/episodeComposition/shows/{show_id}', headers=headers, params=parameters).json()

    for episode in results['episodeList']:
        for media in episode['mediaList']:
            if media['mediaType'] == 'AUDIO' and media['type'] == 'EPISODE':
                return media['id']


def get_episode(media_id: str) -> Episode:
    headers = {
        'Authorization': f'Bearer {get_token()}',
        'Accept': 'application/json'
    }

    parameters = {
        'bu': BU
    }

    results = requests.get(
        url=f'{BASE_URL}/mediaComposition/audios/{media_id}', headers=headers, params=parameters).json()

    show = Show(title=results['show']['title'], description=results['show']
                ['description'], url=results['show']['podcastSubscriptionUrl'])
    medias = [chapter for chapter in results['chapterList']
              if chapter['mediaType'] == 'AUDIO']

    return Episode(show, medias)


def get_podcast_from_show(show_id: str):
    media_id = get_first_media_id(show_id)
    episode = get_episode(media_id)

    p = Podcast(
        name=episode.show.title,
        description=episode.show.description,
        website=episode.show.url,
        explicit=False,
    )

    for media in episode.medias:
        p.episodes.append(podgen.Episode(
            title=media['title'],
            media=Media(media['resourceList'][0]['url']),
        ))

    return p.rss_str()


if __name__ == "__main__":

    shows = {
        'forum': '1784426',
        'la_matinale': '8849020',
        'cqfd': '4191283',
        'le_12h30': '1423859',
        'tout_un_monde': '7006364'
    }

    for name, id in shows.items():
        podcast = get_podcast_from_show(id)
        with open(f'rss/{name}.xml', 'w') as xml:
            xml.write(podcast)
